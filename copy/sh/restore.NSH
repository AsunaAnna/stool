echo -off

cls
##初始化变量
set libdir \efi\lib\ 
set lib_ntfs %libdir%ntfs.efi

set vhddir \workset\disk\ 
set vhdname part_disk.vhdx
set bakvhdname bak_%vhdname%

set vhdpath %vhddir%%vhdname%
set bakvhdpath %vhddir%%bakvhdname%

#流程开始

#检查NTFS库
:checkfile
for %i in fs0 fs1 fs2 fs3 fs4 fs5 fs6 fs7 fs8 fs9 fsa fsb fsc
	if exist %i:\efi\lib\ntfs.efi then
		%i:
		goto loadntfs
	endif
endfor
echo "not find ntfs.efi !!!"
goto end

#还原VHD文件
:checkVHD
for %j in fs0 fs1 fs2 fs3 fs4 fs5 fs6 fs7 fs8 fs9 fsa fsb fsc
	if exist %j:%bakvhdpath% then
		rm %vhdpath%
		cp %bakvhdpath% %vhdpath%
		echo "restore complete!"
		goto end
	endif
endfor
echo "not find vhd file !!!"
goto end

:loadntfs
load %lib_ntfs%
goto checkVHD

:exit
exit

:end
stall 3000000
exit