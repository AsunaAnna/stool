@echo off

rem
rem 命令格式：merge.bat 参数1
rem 参数1：虚拟文件（子文件）绝对地址
rem 

set filecache=aaabbb.txt

echo select vdisk file=%1 > %filecache%
echo merge vdisk depth=1 >> %filecache%
diskpart /s %filecache% 

del %filecache% >nul 2>nul




