@echo off

cls
rem 判断父文件是否存在
call ifno okorno
if not %okorno%==1 ( goto dowork ) else ( goto end )

:dowork
rem 做子差分盘并备份
echo.
echo 正在做子差分盘并备份...
call localfile %vDiskPath%%vDiskPath% vdd
set vDiskPath=%vdd%:%vDiskPath%
del /q %vDiskPath%part_%vdiskFile% >nul 2>nul
call child %vDiskPath%part_%vdiskFile% %vDiskPath%%vdiskFile%
copy /y %vDiskPath%part_%vdiskFile% %vDiskPath%bak_part_%vdiskFile% >nul 2>nul

rem 重新建立引导记录
echo.
echo 重新建立引导记录...
rmdir /S /Q %bootdrive%:\boot >nul 2>nul
call attach %vDiskPath%part_%vdiskFile% %vdiskdriveA%
bcdboot %vdiskdriveA%:\windows /s %bootdrive%:\ >nul
call detach %vDiskPath%part_%vdiskFile%

echo.
echo 系统备份完成!
goto end

:end
echo.



