@echo off

rem
rem 命令格式：create.bat 参数1
rem 参数1：虚拟文件绝对地址
rem 参数2：以MB为单位，设置虚拟文件容量
rem 



set filecache=cbbb.txt

echo create vdisk file=%1 maximum=%2 type=expandable > %filecache%
diskpart /s %filecache%

del %filecache% >nul 2>nul
