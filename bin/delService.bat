@echo off

cd /d %~dp0

cls

title 禁用win服务加速系统运作
echo.
echo 正在禁用服务......
echo.

for /f "tokens=1,2 delims= " %%i in (slist.txt) do (
    call delsgo.bat %%i %%j)


echo.
echo 服务优化完成
echo.

echo.
echo 启用windows照片查看器……
echo.
reg import photo.reg
echo.

pause