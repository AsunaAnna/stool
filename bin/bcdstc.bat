@echo off

if exist %1\EFI\Microsoft\Boot\bcd del %1\EFI\Microsoft\Boot\bcd
bcdedit /createstore %1\EFI\Microsoft\Boot\bcd

bcdedit /store %1\EFI\Microsoft\Boot\bcd /create {bootmgr} /d "Windows Boot Manager"
bcdedit /store %1\EFI\Microsoft\Boot\bcd /set {bootmgr} device partition=c:
bcdedit /store %1\EFI\Microsoft\Boot\bcd /set {bootmgr} locale "zh-CN"



