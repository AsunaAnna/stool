
@echo off 
cls
color 0A 
title IP地址更改小工具 
echo.

:menu
cls
set IP=192.168.0.70
set MASK=255.255.255.0 
set GATEWAY=192.168.0.252
set NAME="Ethernet0"

del /Q /S aab,aac >NUL 2>NUL

echo 本机所有网卡列表：
echo =========================================
ipconfig | findstr /I "适配器">aab
FOR /F "tokens=1* delims= " %%i IN (aab) DO echo %%j>>aac
del /Q /S aab >NUL 2>NUL
FOR /F "tokens=1 delims=:" %%i IN (aac) DO echo %%i>>aab
findstr /N "." aab
echo =========================================

echo.
set /p choice=请选择操作哪张网卡(输入行号)：

findstr /N "." aab|findstr "^%choice%:">aac
FOR /F "tokens=2 delims=:" %%i IN (aac) DO set name=%%i

echo.
echo 你选择的网卡是:%name%

echo. 
echo 自动更改IP 请按 1 
echo 手动更改IP 请按 2 
echo 自动分配IP 请按 3

echo.
set /p KEY= [您的选择是：] 
if %KEY% == 1 goto ONE 
if %KEY% == 2 goto TWO 
if %KEY% == 3 goto three 

:three

echo. & netsh interface ip set address name="%NAME%" source=dhcp
netsh interface ip set dnsservers name="%NAME%" source=dhcp
echo. & echo 按任意键返回继续设置，退出请直接关闭窗口...
pause >nul
goto menu

:TWO 
ECHO 您选择了手工修改设置。 

ECHO. 
echo 回车输入默认IP地址"%IP%"
set /p IP= [请输入IP地址:] 

echo. 
echo 回车输入默认MASK"%MASK%" 
set /p MASK= [请输入 子网掩码 地址:] 

echo. 
echo 回车输入默认GATEWAY"%GATEWAY%"
set /p GATEWAY= [请输入 网关 地址:] 

:ONE 
echo.
echo 请核对以下信息：
echo 网卡:%name%
echo IP:%IP%
echo MASK:%MASK%
echo GATEWAY:%GATEWAY%
echo DNS:%GATEWAY%

echo.
set /p choice=按1重新设置，其他输入将继续：
if %choice% == 1 goto menu 

cls
echo. 
echo 正在自动更改IP...... 
netsh interface ip set address %NAME% static %IP% %MASK% %GATEWAY% 
netsh interface ip set dnsservers %NAME% static %GATEWAY% validate=no

echo IP地址/子网掩码/网关/DNS设置完成 

echo.
echo 按任意键返回继续设置，退出请直接关闭窗口...
del /Q /S aab,aac >NUL 2>NUL
pause >nul
goto menu



