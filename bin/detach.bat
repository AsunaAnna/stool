@echo off

rem 命令格式：detach.bat 参数1
rem 参数1：虚拟文件地址

set filecache=aaabbb.txt

echo select vdisk file=%1 noerr > %filecache%
echo detach vdisk noerr >> %filecache%
diskpart /s %filecache%

del %filecache% >nul 2>nul



