@echo off

rem 命令格式：attach.bat 参数1 参数2
rem 参数1：虚拟文件绝对路径
rem 参数2：挂载的盘符

set filecache=babbb.txt

echo select vdisk file=%1 noerr > %filecache%
echo attach vdisk noerr >> %filecache%
echo select partition 1 >> %filecache%
echo assign letter=%2 >> %filecache%
diskpart /s %filecache% 

del %filecache% >nul 2>nul


