@echo off

rem 设置
rem 参数1 将被打包的磁盘
rem 参数2 WIM文件存放路径

rem imagex /Compress maximum /capture %1 %2 "windows install package"

echo.
echo 请不要主动关闭本窗口，10G系统，打包时间大约2~15分钟之间……
echo.
dism /Capture-Image /ImageFile:%2 /CaptureDir:%1 /Name:SeaulaPackage

echo.
pause