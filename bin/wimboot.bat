REM @echo off

pushd %~dp0
rem 参数1为WIM文件地址
rem 参数2为引导盘符
rem 参数3为新建引导项名称定义

REM specmd file "%~dp0..\copy\efi\bcd=>%2\EFI\Microsoft\Boot\bcd"
REM BCD地址
set bcdfile=%2\efi\microsoft\boot\BCD

REM BCD仓库地址
set bcddir=/store %bcdfile%

rem 预防没有文件目录
mkdir %2\efi\microsoft\boot >nul 2>nul
set ramid=
set osid=
set wimName=%3
set pathd=%2
set pathwim=%1

:lgo
if not exist %bcdfile% (call :createstore)
call :ramdisk %wimName%
call :loader %wimName% %ramid%
rem 复制WIM、SDI文件
specmd file "%pathwim%=>%pathd%\efi\boot\%wimName%.wim"
specmd file "%~dp0..\copy\efi\boot.sdi=>%pathd%\efi\boot\boot.sdi"
rem specmd site %1\grldr,+h+s
goto :end

:createstore
bcdedit /createstore %bcdfile%
bcdedit /store %bcdfile% /create {bootmgr} /d "Seaula Boot Manager"
bcdedit /store %bcdfile% /set {bootmgr} timeout 10
bcdedit /store %bcdfile% /set {bootmgr} locale "zh-CN"
goto :eof

:loader
set osid={%time:~6,2%%time:~9,2%ffff-8d96-11de-8e71-ff%time:~6,2%%time:~9,2%fffffa}
bcdedit %bcddir% /create %osid% /d "%1" /application osloader
bcdedit %bcddir% /set %osid% device ramdisk="[boot]\efi\boot\%1.wim,%2"
bcdedit %bcddir% /set %osid% osdevice ramdisk="[boot]\efi\boot\%1.wim,%2"
rem bcdedit %bcddir% -set %osid% path \windows\system32\winload.efi
rem bcdedit %bcddir% -set %osid% locale zh-CN
bcdedit %bcddir% /set %osid% systemroot \windows
bcdedit %bcddir% /set %osid% detecthal yes
bcdedit %bcddir% /set %osid% winpe yes
bcdedit %bcddir% /set %osid% testsigning yes
bcdedit %bcddir% /set %osid% nointegritychecks yes
bcdedit %bcddir% /set %osid% pae forceenable
bcdedit %bcddir% /set %osid% nx optin
bcdedit %bcddir% /set %osid% locale "zh-CN"
bcdedit %bcddir% /displayorder %osid% /addlast
goto :eof

:ramdisk
set ramid={%time:~6,2%%time:~9,2%ffff-8d96-11de-8e71-%time:~6,2%%time:~9,2%fffffffa}
bcdedit %bcddir% /create %ramid% /d "%1" /device
bcdedit %bcddir% /set %ramid% ramdisksdidevice "boot"
bcdedit %bcddir% /set %ramid% ramdisksdipath \efi\boot\boot.sdi
goto :eof

:end
:exit
rem pause


