@echo off

rem
rem 命令格式：format.bat 参数1
rem 参数1：虚拟文件绝对地址
rem 参数2：磁盘标签
rem 

set filecache=aaabbb.txt

echo select vdisk file=%1 > %filecache%
echo attach vdisk >> %filecache%
echo convert gpt >> %filecache%
echo select partition 1 >> %filecache%
echo delete partition override noerr>> %filecache%
echo create partition primary >> %filecache%
echo format fs=ntfs label=%2 quick >> %filecache%
echo active >> %filecache%
diskpart /s %filecache% >nul

del %filecache% >nul 2>nul



