
@echo off

if exist d:\bcd_tmp del d:\bcd_tmp
bcdedit /createstore d:\bcd_tmp

bcdedit /store d:\bcd_tmp /create {bootmgr} /d "Windows Boot Manager"
bcdedit /store d:\bcd_tmp /set {bootmgr} device partition=c:
bcdedit /store d:\bcd_tmp /set {bootmgr} locale "zh-CN"

REM bcdedit /store d:\bcd_tmp /create {ntldr} /d "WIN10" 
bcdedit /store d:\bcd_tmp /create /d "WIN10" /application osloader 

REM bcdedit /store d:\bcd_tmp /set {%Id%} device partition=%temp%: >nul
REM bcdedit /store d:\bcd_tmp /set {%Id%} path \windows\system32\winload.exe >nul
REM bcdedit /store d:\bcd_tmp /set {%Id%} osdevice partition=%temp%: >nul
REM bcdedit /store d:\bcd_tmp /set {%Id%} systemroot \windows >nul
REM bcdedit /store d:\bcd_tmp /set {%Id%} locale "zh-CN" >nul
REM bcdedit /store d:\bcd_tmp /displayorder {%Id%} /addfirst >nul

REM bcdedit /store d:\bcd_tmp /default {%Id%}
bcdedit /store d:\bcd_tmp
pause