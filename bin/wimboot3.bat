@echo off

rem 参数1为WIM文件地址
rem 参数2为引导盘符
rem 参数3为新建引导项名称定义

REM specmd file "%~dp0..\copy\efi\bcd=>%2\EFI\Microsoft\Boot\bcd"
REM BCD地址
set bcdfile=%2\efi\microsoft\boot\BCD

REM BCD仓库地址
set bcddir=/store %bcdfile%

rem 预防没有文件目录
mkdir %2\efi\microsoft\boot

if not exist %bcdfile% (goto createstore) else (goto insert)

:createstore
bcdedit /createstore %bcdfile%
bcdedit /store %bcdfile% /create {bootmgr} /d "Windows Boot Manager"
bcdedit /store %bcdfile% /set {bootmgr} device partition=%2
bcdedit /store %bcdfile% /set {bootmgr} timeout 10
bcdedit /store %bcdfile% /set {bootmgr} locale "zh-CN"

:insert
bcdedit %bcddir% /create {ramdiskoptions} /d "Ramdisk options"
bcdedit %bcddir% /set {ramdiskoptions} ramdisksdidevice "boot"
bcdedit %bcddir% /set {ramdiskoptions} ramdisksdipath \efi\boot\boot.sdi

rem for /f "delims={,} tokens=2" %%a in ('bcdedit %bcddir% /create /d "%3" -application osloader  ') do set ID={%%a}
set id={%time:~6,2%%time:~9,2%ffff-8d96-11de-8e71-fffffffffffa}

bcdedit %bcddir% /create %id% /d "%3" /application osloader

bcdedit %bcddir% /set %ID% device ramdisk="[boot]\efi\boot\%3.wim,{ramdiskoptions}"
bcdedit %bcddir% /set %ID% osdevice ramdisk="[boot]\efi\boot\%3.wim,{ramdiskoptions}"
rem bcdedit %bcddir% -set %ID% path \windows\system32\winload.efi
rem bcdedit %bcddir% -set %ID% locale zh-CN
bcdedit %bcddir% /set %ID% systemroot \windows
bcdedit %bcddir% /set %ID% detecthal yes
bcdedit %bcddir% /set %ID% winpe yes
bcdedit %bcddir% /set %ID% testsigning yes
bcdedit %bcddir% /set %ID% nointegritychecks yes
bcdedit %bcddir% /set %ID% pae forceenable
bcdedit %bcddir% /set %ID% nx optin
bcdedit %bcddir% /displayorder %ID% /addlast

rem 复制WIM、SDI文件
specmd file "%1=>%2\efi\boot\%3.wim"
specmd file "%~dp0..\copy\efi\boot.sdi=>%2\efi\boot\boot.sdi"

rem specmd site %1\grldr,+h+s

:exit
rem pause


