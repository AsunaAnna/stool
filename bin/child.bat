@echo off

rem
rem 命令格式：child.bat 参数1 参数1
rem 参数1：虚拟文件（子文件）绝对地址
rem 参数2：虚拟文件（父文件）绝对地址
rem 

set filecache=aaabbb.txt

echo create vdisk file=%1 parent=%2 noerr > %filecache%
diskpart /s %filecache%

del %filecache% >nul 2>nul



